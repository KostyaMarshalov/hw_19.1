﻿
#include <iostream>
#include<string>

using namespace std;

class Animal
{
   
    public:
       virtual void Voice()
        {
            cout << "Animal voice" << endl;
        }
       virtual ~Animal() 
        { 
           cout << "~Animal()" << endl; 
        }
};

class Dog : public Animal
{
    void Voice() override
    {
        cout << " Woof! " << endl;
    }
    ~Dog()
     {
        cout << "~Dog()" << endl;
     }
};

class Cat : public Animal
{
    void Voice() override
    {
        cout << " Meow! " << endl;
    }
    ~Cat()
    {
        cout << "~Cat()" << endl;
    }
};

class Horse : public Animal
{
    void Voice() override
    {
        cout << " Igogo! " << endl;
    }
    ~Horse()
    {
        cout << "~Horse()" << endl;
    }
};

int main()
{
    Animal* p1 = new Dog();
    p1->Voice();
    delete p1;
    Animal* p2 = new Cat();
    p2->Voice();
    delete p2;
    Animal* p3 = new Horse();
    p3->Voice();
    delete p3;

    int size;
    cout << "Input size " << endl;
    cin >> size;
    int index = 0;
    Animal **arr = new Animal* [size];

    for (index = 0; index < size; index++)
    {
        char choise;
        cout << "Input choise from 1 to 3  " << endl;
        cin >> choise;
        switch (choise)
        {
        
        case '1': arr[index] = new Dog; break; 
        case '2': arr[index] = new Cat; break;
        case '3': arr[index] = new Horse; break;

            
        }
       // arr[index] = rand() % 3;
    }

    for (index = 0; index < size; index++)
    {
        arr[index] ->Voice();
        
        delete arr[index];
    }

    delete[] arr;

   


    return 0;
}


